Mancala UI

UI implementation of Mancala game
Tools and Technologies used

Angular 8
HTML
CSS

To-Do / Issues
Authentication
How to run in local
To build the application : npm install
To start the application : ng serve and access through browser http://localhost:4200/
Deployment
Deployed on AWS S3 public bucket.
url: http://mancala-ui.s3-website.us-east-2.amazonaws.com/
Dependency
Requires Mancala Backend implementation https://gitlab.com/ahmedhamed.0026/mancala