import { Component } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { GameState } from './game-state';
import { Player } from './player';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { WebSocketAPI } from './app.websocket';
import {Observable} from "rxjs/Observable";
import * as Immutable from 'immutable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mancala Game';

  showPlayer1:boolean;
  showPlayer2:boolean;
  gameId:String;
  player1Name:String;
  player2Name:String;
  board:number[];
  gameStatus:String;
  message:String;
  gameState:GameState;
  stompClient:any;
  webSocketAPI:WebSocketAPI
  boardMap:any
  ngOnInit() {
    this.webSocketAPI = new WebSocketAPI(new AppComponent());
  }
  constructor(){
    this.showPlayer1=false;
    this.showPlayer2=true;
    this.player1Name="Player 1";
    this.player2Name="Player 2";
    this.boardMap = Immutable.Map<number,number>();

    for(let i =0;i<13;i++){
    this.boardMap =  this.boardMap.set(''+i,'6');
    }
    this.boardMap = this.boardMap.set('6','0');
    this.boardMap = this.boardMap.set('13','0');

  }



  delay(milliSeconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliSeconds) );
  }


  joinGame(){

  }

  instructions(){

  }




  newGame(){
     this.webSocketAPI._connect();
  }
  play(index:number){
  let message = this.webSocketAPI._send(index);
   this.webSocketAPI.boardMap$.subscribe(boardMap => this.boardMap = boardMap  );
  }



} 
