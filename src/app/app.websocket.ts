import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { AppComponent } from './app.component';
import  {Component, Input} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import * as Immutable from 'immutable';
import * as uuid from 'uuid';
export class WebSocketAPI {
//     webSocketEndPoint: string = 'http://3.137.168.116:8080/mancala/create-game';
    webSocketEndPoint: string = 'http://localhost:8080/mancala/create-game';
    subscribe: string = "/sub-play";
    stompClient: any;
    appComponent: AppComponent;
    Board:any;
    gameId:number;
   private boardMap: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
   public boardMap$: Observable<any[]> = this.boardMap.asObservable();

    constructor(appComponent: AppComponent){

        this.appComponent = appComponent;
    }
    _connect() {
        console.log("Initialize WebSocket Connection");
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.gameId = uuid.v4();
        _this.stompClient.connect({}, function (frame) {
            _this.stompClient.subscribe(_this.subscribe, function (sdkEvent) {
                _this.onMessageReceived(sdkEvent);
            });
            //_this.stompClient.reconnect_delay = 2000;
        }, this.errorCallBack);
    };

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    // on error, schedule a reconnection attempt
    errorCallBack(error) {
        setTimeout(() => {
            this._connect();
        }, 5000);
    }

	/**
	 * Send message to sever via web socket
	 * @param {*} message
	 */
    _send(pitId) {
    pitId = JSON.stringify(pitId);
        let gamePlay = '{"gameId":"'+this.gameId+
                        '","pitId":"'+pitId+'"}';
        this.stompClient.send("/play", {}, (gamePlay));
        return pitId;
    }

    onMessageReceived(message)  {
        console.log("Message Recieved from Server :: " + message.body);
        let map  = JSON.parse(message.body) ;
         this.gameId = map.gameId;
        map = map.gameMap

        this.Board =  Immutable.Map<any,any>();
            for (let key in map){
                this.Board = this.Board.set(key,JSON.stringify(map[key]));
            }
        this.boardMap.next(this.Board);

    }
    }
